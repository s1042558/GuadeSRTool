
# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GuadRSTool
                                 A QGIS plugin
 IP3 Project: Relocation of endangered inhabitants of Guadeloupe.
                              -------------------
        begin                : 2017-11-20
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Recep Alkaya / Xavier Desrousseaux
        email                : S1042558@stud.sbg.ac.at
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication
from PyQt4.QtGui import QAction, QIcon, QFileDialog, QMessageBox
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from GuadRSTool_dialog import GuadRSToolDialog
import os.path
from osgeo import gdal
from osgeo import ogr
import osr
from math import ceil
import datetime
import sys
import shutil
from qgis.core import *



# Initialize QGIS Application
QgsApplication.setPrefixPath("C:\\OSGeo4W64\\apps\\qgis", True)
app = QgsApplication([], True)
QgsApplication.initQgis()

# Add the path to Processing framework
sys.path.append('C:\\Program Files\\QGIS 2.18\\apps\\qgis\\python\\plugins')

# Import and initialize Processing framework
from processing.core.Processing import Processing

Processing.initialize()
import processing




class GuadRSTool:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'GuadRSTool_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)


        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&GuadRSTool')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'GuadRSTool')
        self.toolbar.setObjectName(u'GuadRSTool')

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('GuadRSTool', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        # Create the dialog (after translation) and keep reference
        self.dlg = GuadRSToolDialog()

        # Elevation connection to pushbutton
        self.dlg.lineEdit_elevation.clear()
        self.dlg.pushButton_elevation.clicked.connect(self.select_elevation)

        # urbanplan connection to pushbutton
        self.dlg.lineEdit_urbanplan.clear()
        self.dlg.pushButton_urbanplan.clicked.connect(self.select_urbanplan)

        # riskplan connection to pushbutton
        self.dlg.lineEdit_riskplan.clear()
        self.dlg.pushButton_riskplan.clicked.connect(self.select_riskplan)

        # coastline connection to pushbutton
        self.dlg.lineEdit_coastline.clear()
        self.dlg.pushButton_coastline.clicked.connect(self.select_coastline)

        # coastline connection to pushbutton
        self.dlg.lineEdit_buildings.clear()
        self.dlg.pushButton_buildings.clicked.connect(self.select_buildings)

        # output
        self.dlg.lineEdit_output.clear()
        self.dlg.pushButton_output.clicked.connect(self.save_output)

        # output
        self.dlg.lineEdit_temp.clear()
        self.dlg.pushButton_temp.clicked.connect(self.select_temp)


        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/GuadRSTool/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'GuadRSTool'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&GuadRSTool'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar

	#Functions for selection data path
    def select_elevation(self):
        filename = QFileDialog.getOpenFileName(self.dlg, "Select elevation file", "", '*.jpg;*.tif')
        self.dlg.lineEdit_elevation.setText(filename)

    def select_urbanplan(self):
        filename = QFileDialog.getOpenFileName(self.dlg, "Select urban plan file", "", '*.shp')
        self.dlg.lineEdit_urbanplan.setText(filename)

    def select_riskplan(self):
        filename = QFileDialog.getOpenFileName(self.dlg, "Select risk plan file", "", '*.shp')
        self.dlg.lineEdit_riskplan.setText(filename)

    def select_coastline(self):
        filename = QFileDialog.getOpenFileName(self.dlg, "Select coastline file", "", '*.shp')
        self.dlg.lineEdit_coastline.setText(filename)

    def select_buildings(self):
        filename = QFileDialog.getOpenFileName(self.dlg, "Select buildings file", "", '*.shp')
        self.dlg.lineEdit_buildings.setText(filename)

    def select_buildings(self):
        filename = QFileDialog.getOpenFileName(self.dlg, "Select buildings file", "", '*.shp')
        self.dlg.lineEdit_buildings.setText(filename)

    def save_output(self):
        filename = QFileDialog.getSaveFileName(self.dlg, "Save output file", "", '*.shp')
        self.dlg.lineEdit_output.setText(filename)

    def select_temp(self):
        filename = QFileDialog.getExistingDirectory(self.dlg, "Select temp folder","")
        self.dlg.lineEdit_temp.setText(filename)


    def run(self):

        """Run method that performs all the real work"""
        # show the dialog
        self.dlg.show()

        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.

            # Opening pathes got by lineEdit
            dem = str(self.dlg.lineEdit_elevation.text())
            urbanplan = str(self.dlg.lineEdit_urbanplan.text())
            riskplan = str(self.dlg.lineEdit_riskplan.text())
            coastline = str(self.dlg.lineEdit_coastline.text())
            building = str(self.dlg.lineEdit_buildings.text())
            output = str(self.dlg.lineEdit_output.text())
            temp = str(self.dlg.lineEdit_temp.text()) + "\\temp"

            # Calculate Weighting if 100% given or not
            InEle = int(str(self.dlg.lineEdit_InEle.text()))
            InSlo = int(str(self.dlg.lineEdit_InSlo.text()))
            InUrb = int(str(self.dlg.lineEdit_InUrb.text()))
            InBui = int(str(self.dlg.lineEdit_InBui.text()))
            InCoa = int(str(self.dlg.lineEdit_InCoa.text()))
            InRis = int(str(self.dlg.lineEdit_InRis.text()))

            sumInfluence = InEle + InSlo + InUrb + InBui + InCoa + InRis

            if sumInfluence != 100:
                QMessageBox.information(None, "Info:",
                                        str("Influence has to be 100% in sum! Please try it again!"))  # Info display

            else:
                # Create Temp folder
                if not os.path.exists(temp):
                    os.makedirs(temp)

                # Create Slope of DEM
                slope = temp + "\\Slope.tif"
                processing.runalg('gdalogr:slope', dem, 1, False, False, False, 1, slope)

                # Polygonzize of DEM
                raster = gdal.Open(dem)
                band = raster.GetRasterBand(1)

                # Raster Information
                geoTransform = raster.GetGeoTransform()
                minx = geoTransform[0]
                maxy = geoTransform[3]
                maxx = minx + geoTransform[1] * raster.RasterXSize
                miny = maxy + geoTransform[5] * raster.RasterYSize
                RasterX = raster.RasterXSize
                RasterY = raster.RasterYSize

                srs = osr.SpatialReference()
                srs.ImportFromEPSG(32620)

                path = temp + "\\DemPoly.shp"
                driver = ogr.GetDriverByName("ESRI Shapefile")
                if os.path.exists(path):
                    driver.DeleteDataSource(path)
                outDatasource = driver.CreateDataSource(path)
                outLayer = outDatasource.CreateLayer("polygonized", srs=srs)
                newField = ogr.FieldDefn('RasterVal', ogr.OFTInteger)
                outLayer.CreateField(newField)

                gdal.Polygonize(band, None, outLayer, 0, [], callback=None)
                outDatasource.Destroy()
                dem = path  # Update path of dem

                # Polygonzize of Slope
                raster = gdal.Open(slope)
                band = raster.GetRasterBand(1)
                srs = osr.SpatialReference()
                srs.ImportFromEPSG(32620)

                path = temp + "\\SlopePoly.shp"
                driver = ogr.GetDriverByName("ESRI Shapefile")
                if os.path.exists(path):
                    driver.DeleteDataSource(path)
                outDatasource = driver.CreateDataSource(path)
                outLayer = outDatasource.CreateLayer("polygonized", srs=srs)
                newField = ogr.FieldDefn('RasterVal', ogr.OFTInteger)
                outLayer.CreateField(newField)

                gdal.Polygonize(band, None, outLayer, 0, [], callback=None)
                outDatasource.Destroy()
                slope = path  # Update path of slope

                # Creating Fishnet on DEM Extend
                path = temp + "\\fishnet30x30.shp"

                # convert sys.argv to float
                xmin = float(minx)
                xmax = float(maxx)
                ymin = float(miny)
                ymax = float(maxy)
                gridWidth = float(30)
                gridHeight = float(30)

                # print [xmin, xmax, ymin, ymax]
                # print [gridWidth, gridHeight]

                # get rows
                rows = ceil((ymax - ymin) / gridHeight)
                # get columns
                cols = ceil((xmax - xmin) / gridWidth)

                # start grid cell envelope
                ringXleftOrigin = xmin
                ringXrightOrigin = xmin + gridWidth
                ringYtopOrigin = ymax
                ringYbottomOrigin = ymax - gridHeight

                # create output file
                outDriver = ogr.GetDriverByName('ESRI Shapefile')
                if os.path.exists(path):
                    os.remove(path)
                outDataSource = outDriver.CreateDataSource(path)
                outLayer = outDataSource.CreateLayer(path, geom_type=ogr.wkbPolygon, srs=srs)
                featureDefn = outLayer.GetLayerDefn()

                # create grid cells
                countcols = 0
                while countcols < cols:
                    countcols += 1

                    # reset envelope for rows
                    ringYtop = ringYtopOrigin
                    ringYbottom = ringYbottomOrigin
                    countrows = 0

                    while countrows < rows:
                        countrows += 1
                        ring = ogr.Geometry(ogr.wkbLinearRing)
                        ring.AddPoint(ringXleftOrigin, ringYtop)
                        ring.AddPoint(ringXrightOrigin, ringYtop)
                        ring.AddPoint(ringXrightOrigin, ringYbottom)
                        ring.AddPoint(ringXleftOrigin, ringYbottom)
                        ring.AddPoint(ringXleftOrigin, ringYtop)
                        poly = ogr.Geometry(ogr.wkbPolygon)
                        poly.AddGeometry(ring)

                        # add new geom to layer
                        outFeature = ogr.Feature(featureDefn)
                        outFeature.SetGeometry(poly)
                        outLayer.CreateFeature(outFeature)
                        outFeature.Destroy

                        # new envelope for next poly
                        ringYtop = ringYtop - gridHeight
                        ringYbottom = ringYbottom - gridHeight

                    # new envelope for next poly
                    ringXleftOrigin = ringXleftOrigin + gridWidth
                    ringXrightOrigin = ringXrightOrigin + gridWidth

                # Close DataSources
                outDataSource.Destroy()
                fishnet = path

                # Buffer of Coastline
                Coastline60 = temp + "\\CBE_buffer60.shp"
                NumDep = 60
                processing.runalg("qgis:fixeddistancebuffer", coastline, NumDep, 5, True, Coastline60)
                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Coastline60, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('NumDep', ogr.OFTInteger)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("NumDep", NumDep)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()
                dataSource.Destroy()

                Coastline100 = temp + "\\CBE_buffer100.shp"
                NumDep = 100
                processing.runalg("qgis:fixeddistancebuffer", coastline, NumDep, 5, True, Coastline100)
                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Coastline100, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('NumDep', ogr.OFTInteger)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("NumDep", NumDep)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()
                dataSource.Destroy()

                Coastline15000 = temp + "\\CBE_buffer15000.shp"
                NumDep = 15000
                processing.runalg("qgis:fixeddistancebuffer", coastline, NumDep, 5, True, Coastline15000)
                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Coastline15000, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('NumDep', ogr.OFTInteger)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("NumDep", NumDep)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()
                dataSource.Destroy()

                Output1 = temp + "\\CBE_bufferUnion1.shp"
                processing.runalg('qgis:difference', Coastline100, Coastline60, False, Output1)

                Output2 = temp + "\\CBE_bufferUnion2.shp"
                processing.runalg('qgis:difference', Coastline15000, Coastline100, False, Output2)

                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Output2, 1)
                layer = dataSource.GetLayer()
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("NumDep", 15000)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()
                dataSource.Destroy()

                in1 = temp + "\\CBE_buffer60.shp"
                in2 = temp + "\\CBE_bufferUnion1.shp"
                in12 = in1 + ";" + in2
                Merge1 = temp + "\\CBE_bufferMerge1.shp"
                processing.tools.general.runalg("qgis:mergevectorlayers",
                                                in12,
                                                Merge1)

                in1 = temp + "\\CBE_buffer60.shp"
                in2 = temp + "\\CBE_bufferUnion2.shp"
                in12 = in1 + ";" + in2
                coastline = temp + "\\CBE_bufferMerge1.shp"
                processing.tools.general.runalg("qgis:mergevectorlayers",
                                                in12,
                                                coastline)

                # Buffer of Building
                Builtup3000 = temp + "\\built3000.shp"

                processing.runalg("qgis:fixeddistancebuffer", building, 3000, 5, True, Builtup3000)
                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Builtup3000, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('buildup', ogr.OFTInteger)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("buildup", 3000)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                Builtup7000 = temp + "\\built7000.shp"

                processing.runalg("qgis:fixeddistancebuffer", building, 7000, 5, True, Builtup7000)
                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Builtup7000, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('buildup', ogr.OFTInteger)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("buildup", 7000)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                Builtup15000 = temp + "\\built15000.shp"

                processing.runalg("qgis:fixeddistancebuffer", building, 15000, 5, True, Builtup15000)
                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Builtup15000, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('buildup', ogr.OFTInteger)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("buildup", 15000)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                Output1 = temp + "\\Builtup_dif7000.shp"
                processing.runalg('qgis:difference', Builtup7000, Builtup3000, False, Output1)

                Output2 = temp + "\\Builtup_dif15000.shp"
                processing.runalg('qgis:difference', Builtup15000, Builtup7000, False, Output2)

                driver = ogr.GetDriverByName('ESRI Shapefile')
                dataSource = driver.Open(Output2, 1)
                layer = dataSource.GetLayer()
                feature = layer.GetNextFeature()

                while feature:
                    feature.SetField("buildup", 15000)
                    layer.SetFeature(feature)
                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                in1 = temp + "\\built3000.shp"
                in2 = temp + "\\Builtup_dif7000.shp"
                in12 = in1 + ";" + in2
                Output3 = temp + "\\Builtbuffer_Merge2.shp"
                processing.tools.general.runalg("qgis:mergevectorlayers",
                                                in12,
                                                Output3)

                in1 = temp + "\\Builtbuffer_Merge2.shp"
                in2 = temp + "\\Builtup_dif15000.shp"
                in12 = in1 + ";" + in2
                building = temp + "\\Builtbuffer_Merge3.shp"
                processing.tools.general.runalg("qgis:mergevectorlayers",
                                                in12,
                                                building)

                # Spatial Join
                processing.runalg("qgis:joinattributesbylocation", fishnet, dem, u'intersects', 0, 0, 'sum', 1,
                                  temp + "/join1.shp")
                join1 = temp + "\\join1.shp"
                processing.runalg("qgis:joinattributesbylocation", join1, slope, u'intersects', 0, 0, 'sum', 1,
                                  temp + "/join2.shp")
                join2 = temp + "\\join2.shp"
                processing.runalg("qgis:joinattributesbylocation", join2, building, u'intersects', 0, 0, 'sum', 1,
                                  temp + "/join3.shp")
                join3 = temp + "\\join3.shp"
                processing.runalg("qgis:joinattributesbylocation", join3, coastline, u'within', 0, 0, 'sum', 1,
                                  temp + "/join4.shp")
                join4 = temp + "\\join4.shp"
                processing.runalg("qgis:joinattributesbylocation", join4, urbanplan, u'intersects', 0, 0, 'sum', 1,
                                  temp + "/join5.shp")
                join5 = temp + "\\join5.shp"
                processing.runalg("qgis:joinattributesbylocation", join5, riskplan, u'intersects', 0, 0, 'sum', 1,
                                  output)

                # reclassfication
                driver = ogr.GetDriverByName('ESRI Shapefile')

                ##### Reclassification of Coastline
                Inf = InCoa * 6.0 / 100.0
                dataSource = driver.Open(output, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('Coastline', ogr.OFTReal)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    if feature.GetField("NumDep") > 60 and feature.GetField("NumDep") <= 100:
                        feature.SetField("Coastline", 1.0 * Inf)
                        layer.SetFeature(feature)

                    elif feature.GetField("NumDep") > 100:
                        feature.SetField("Coastline", 3.0 * Inf)
                        layer.SetFeature(feature)

                    else:
                        feature.SetField("Coastline", -999)
                        layer.SetFeature(feature)

                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                ##### Reclassification of Urbanplan
                Inf = InUrb * 6.0 / 100.0
                dataSource = driver.Open(output, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('Urbanplan', ogr.OFTReal)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    if feature.GetField("TYPEZONE") == "U" or feature.GetField("TYPEZONE") == "AUc":
                        feature.SetField("Urbanplan", 3.0 * Inf)
                        layer.SetFeature(feature)

                    else:
                        feature.SetField("Urbanplan", -999)
                        layer.SetFeature(feature)

                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                ##### Reclassification of Riskplan
                Inf = InRis * 6.0 / 100.0
                dataSource = driver.Open(output, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('Riskplan', ogr.OFTReal)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    if feature.GetField("contrainte") == "contraintes courantes" or feature.GetField(
                            "contrainte") == "inconnu":
                        feature.SetField("Riskplan", 1.0 * Inf)
                        layer.SetFeature(feature)

                    elif feature.GetField("contrainte") == "contraintes moyennes":
                        feature.SetField("Riskplan", 2.0 * Inf)
                        layer.SetFeature(feature)

                    elif feature.GetField("contrainte") == "contraintes faibles":
                        feature.SetField("Riskplan", 3.0 * Inf)
                        layer.SetFeature(feature)

                    else:
                        feature.SetField("Riskplan", -999)
                        layer.SetFeature(feature)

                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                ##### Reclassification of Buildings
                Inf = InBui * 6.0 / 100.0
                dataSource = driver.Open(output, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('Buildings', ogr.OFTReal)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    if feature.GetField("buildup") > 7000:
                        feature.SetField("Buildings", 1.0 * Inf)
                        layer.SetFeature(feature)

                    elif feature.GetField("buildup") <= 7000 and feature.GetField("buildup") > 3000:
                        feature.SetField("Buildings", 2.0 * Inf)
                        layer.SetFeature(feature)

                    elif feature.GetField("buildup") <= 3000:
                        feature.SetField("Buildings", 3.0 * Inf)
                        layer.SetFeature(feature)

                    else:
                        feature.SetField("Buildings", -999)
                        layer.SetFeature(feature)

                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                # Reclassification of DEM
                Inf = InEle * 6.0 / 100.0
                dataSource = driver.Open(output, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('DEM', ogr.OFTReal)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    if feature.GetField("RasterVal") < 10:
                        feature.SetField("DEM", -999)
                        layer.SetFeature(feature)

                    elif feature.GetField("RasterVal") >= 10 and feature.GetField("RasterVal") < 15:
                        feature.SetField("DEM", 1.0 * Inf)
                        layer.SetFeature(feature)

                    elif feature.GetField("RasterVal") >= 15 and feature.GetField("RasterVal") < 20:
                        feature.SetField("DEM", 2.0 * Inf)
                        layer.SetFeature(feature)

                    else:
                        feature.SetField("DEM", 3.0 * Inf)
                        layer.SetFeature(feature)

                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                # Reclassification of Slope
                Inf = InSlo * 6.0 / 100.0
                dataSource = driver.Open(output, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('Slope', ogr.OFTReal)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    if feature.GetField("RasterVa_1") > 10:
                        feature.SetField("Slope", -999)
                        layer.SetFeature(feature)

                    elif feature.GetField("RasterVa_1") >= 8 and feature.GetField("RasterVa_1") <= 10:
                        feature.SetField("Slope", 1.0 * Inf)
                        layer.SetFeature(feature)

                    elif feature.GetField("RasterVa_1") >= 4 and feature.GetField("RasterVa_1") < 8:
                        feature.SetField("Slope", 2.0 * Inf)
                        layer.SetFeature(feature)

                    else:
                        feature.SetField("Slope", 3.0 * Inf)
                        layer.SetFeature(feature)

                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                # Concatenation
                dataSource = driver.Open(output, 1)
                layer = dataSource.GetLayer()
                new_field = ogr.FieldDefn('Concatenat', ogr.OFTReal)
                layer.CreateField(new_field)
                feature = layer.GetNextFeature()

                while feature:
                    conc = feature.GetField("Coastline") + feature.GetField("Urbanplan") + feature.GetField(
                        "Buildings") + feature.GetField("DEM") + feature.GetField("Slope") + feature.GetField(
                        "Riskplan")

                    if conc > 0:
                        feature.SetField("Concatenat", conc)
                        layer.SetFeature(feature)
                    else:
                        feature.SetField("Concatenat", 0)
                        layer.SetFeature(feature)

                    feature = layer.GetNextFeature()

                dataSource.Destroy()

                shutil.rmtree(temp)

                QMessageBox.information(None, "Info:", "Process finished successfully!") #Info display